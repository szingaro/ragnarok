\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {chapter}{\numberline {2}Methods}{5}
\contentsline {section}{\numberline {2.1}Modeling Secondary Structure \\with SCFGs}{5}
\contentsline {subsection}{\numberline {2.1.1}Architecture}{6}
\contentsline {subsection}{\numberline {2.1.2}Scoring Scheme}{8}
\contentsline {subsection}{\numberline {2.1.3}Parameterization}{9}
\contentsline {subsection}{\numberline {2.1.4}Folding Algorithm}{9}
\contentsline {section}{\numberline {2.2}GRNApred}{11}
\contentsline {subsection}{\numberline {2.2.1}Training Procedure}{11}
\contentsline {subsection}{\numberline {2.2.2}Testing Procedure}{12}
\contentsline {section}{\numberline {2.3}Evaluation Measures}{12}
\contentsline {chapter}{\numberline {3}Experimentation}{15}
\contentsline {section}{\numberline {3.1}Purpose}{15}
\contentsline {section}{\numberline {3.2}Conditions}{16}
\contentsline {subsection}{\numberline {3.2.1}Dataset and Ranges}{16}
\contentsline {subsection}{\numberline {3.2.2}TORNADO Grammars}{17}
\contentsline {subsection}{\numberline {3.2.3}Hardware}{18}
\contentsline {subsection}{\numberline {3.2.4}Parameters}{18}
\contentsline {chapter}{\numberline {4}Results}{19}
\contentsline {section}{\numberline {4.1}Analysis of Sequence Length}{20}
\contentsline {section}{\numberline {4.2}Analysis of RNA type}{21}
\contentsline {section}{\numberline {4.3}Analysis of GC Content}{22}
\contentsline {chapter}{\numberline {5}Conclusions}{23}
