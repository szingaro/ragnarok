\chapter{Methods}
\label{sec:1}
In this chapter are described tools and methods used to rank predictors of RNA secondary structure and are motivated the choices of the components used to predict single sequence RNA molecule.
First the ranking method, called GRNApred is described, consisting of two procedures: the training builds the rankings of grammars based on sequence-derived features; the testing actually performs the prediction on a blind target sequence. 
This work is intended to validate the training procedure.
\section{Modeling Secondary Structure \\with SCFGs}
\label{sec:1.1}
Any method for RNA secondary structure prediction is determined by four components: architecture, scoring scheme, parameterization and inference method. 
The architecture consists of the list of features selected which, in turn, determine the number of parameters of the model.
It has been shown in \cite{Rivas:2012} that all the different architectures one can devise for a nested (non pseudo-knotted) RNA secondary structure all fall into the category of Context-Free Grammars (CFGs), where grammars are intended in the Chomsky sense \cite{Chomsky:1959}.
Any architecture can be implemented using thermodynamics, weights or probabilistic parameters. 
Both weights and probabilistic schemes are trained on data and we refer to them as statistical schemes. 
There are statistical weight schemes such as Conditional Log-Linear Models (CLLMs) and statistical probabilistic schemes such as Stochastic CFGs (SCFGs).
Notice that SCFGs only describe models with a probabilistic scheme, while the concept of CFG applies to all scoring schemes.
The assignment of values for the parameters depends on the scheme used.
Thermodynamic models take values as kcal/mol free-energy estimations from experimental data.
Probabilistic models are usually trained by maximum likelihood methods, which require obtaining frequencies of occurrences in the training set.
These ingredients are referred to as the model.
The fourth component is the folding algorithm, used to predict plausible secondary structures given the model and the sequence of a structural RNA.
Unlike training, which is specific for each different scoring schemes, the inference folding algorithms are essentially identical for all parameterization and rely on the concepts of dynamic programming (DP).
\subsection{Architecture}
\label{sec:3}
RNA secondary structure is defined by the hydrogen bond interactions between the Watson-Crick faces (in cis) of the two nucleotides located at arbitrary distance apart in the RNA backbone \cite{Rivas:2013}, these interactions are referred to as canonical base pairs. 
Alternative hydrogen bonding patterns can occur between other faces (there are three per nucleotides Watson-Crick, Hoogsteen and Sugar-edge in cis or trans) giving rise to non-canonical base pairs \cite{Leontis:2001}, and, in turn, they determine the tertiary structure of the molecule.
In order to stabilize the molecule canonical base pairs can occur in conjunction forming short helices(stems).
Unpaired nucleotides that interrupt helices are called bulges or internal loops, depending on the number of those.
Most RNA helices are nested within each other tending to aggregate themselves next to each other in crystal structure, stacked coaxially forming longer stems.
However, a small fraction of base pairs appears in non-nested configurations called pseudoknots.
This work focus on a method that do not take in account this peculiar type of interactions. 
Although one should not forget that important functional properties of RNA molecule may arise exactly from this non-nested structures.
An important advance in methods for prediction of RNA secondary structure was the realization that, since RNA folding could be represented as Context-Free Grammar (CFG), RNA secondary structure prediction could be viewed as a CFG parsing problem \cite{Durbin:1998}.
A CFG consists of non-terminals (NTs), terminals, and production rules of the form $[NT\rightarrow (\text{any combination of NTs/terminals})]$.
The production rules determine recursively the strings of RNA bases and structures that the grammar permits \cite{Hopcroft:1979}.
Grammar for RNA folding allows all possible strings of nucleotides, imposing restriction on secondary structures allowed, but they weight each string differently according to the scoring system that assign values to the parameters of the grammar.
Grammar parameters that provide score for the single nucleotide are named emissions.
Parameters that weight the different rules for a given non-terminal are named transitions.
The TORNADO code for unambiguous grammar that incorporates loops and stacking shown in figure \ref{fig:1} has eight non-terminals.
Non-terminal F corresponds to an helix whilst P corresponds to all possible types of loops.
This basic\_grammar is the core of both ViennaRNA and CONTRAfold before terminal mismatches and dangles were added.
There are eight non-terminal and nineteen rules, eleven of which include emissions terminals. 
Seven of this emission terminals use a total of thirty five predefined residue emission distribution.
Residue emission distribution with different attributes, as the number of residues, the context or the base pairs, can have the same name. There are four loops emitting terminals for hairpins, left and right bulges, and terminal loops, which use three length distribution (l1, l2, l3 respectively).
The length distribution have a maximum number of emissions determined by P\_MAX\_LENGTH, and a maximum number  of residues controlled by P\_FIT\_LENGTH after which emissions are fitted to a simple model. 
The total number of free tied parameter for this grammar is 1022. 
\subsection{Scoring Scheme}
\label{sec:4}
In a probabilistic scoring scheme it is considered the probability of producing a specific base pair versus the probability of any other ones.
Those probabilities could be selected by hand, for instance, $P_{AU}=P_{UA}=P_{CG}=P_{GC}=\frac{1}{4}$ if we discard $G-U$ and $U-G$ pairs, assuming that all the others are equally likely. 
Normally, these probabilities are estimated from large numbers of known RNA structures.
Technically in a probabilistic scheme, the transitions associated to a given non-terminal define a probability distribution.
Similarly, for each rule, if there are nucleotides being emitted, an emission probability distribution needs to be defined. 
Stochastic context-free grammars were first suggested for single-sequence RNA secondary structure prediction in \cite{Durbin:1998}, following from their earlier use in consensus structure prediction seen in \cite{Eddy:1994}.
The reasons for favoring a probabilistic scoring scheme instead of a weighted or thermodynamic one follow that devised in \cite{Rivas:2013}:
\begin{enumerate}
  \item Probabilistic method, by being generative, allow to do basic sanity checks on the biological meaning of the RNA structure produced in the prediction
  \item Probabilistic method a ideal in combining different source of informations together, important in the sense that RNA comprise several feature that should be included in the model for structures predictions
  \item A probabilistic model is easy to train, it only needs a good training dataset
\end{enumerate}
Another important reason is that it is possible to get insight into relevant feature of RNA folding, that might have gone unnoticed under other scoring schemes. For instance, long range features of RNA secondary structure only become apparent after mapping the nearest-neighbor thermodynamic model into a grammar architecture.
Those additional parameters are usually transition parameters in the grammar formalism.
These transition parameters are usually hard to determine by thermodynamic experimentation with small synthetic oligonucleotides.
SCFGs use a probabilistic scoring scheme and is a generative method, that calculates the joint distribution of sequence and structure given the model.
\begin{equation}
P(s,\pi_s|M)
\end{equation}
\subsection{Parameterization}
\label{sec:5}
The training method used for generative models is a maximum likelihood (ML).
The estimation of the parameters corresponds to optimizing for a given set of sequences and structures the sum of all joint probabilities.
\begin{equation}
\sum_s{\log{P(s,\pi_s|M)}}
\end{equation}
Under the probabilistic constraint $\sum_s{t_\alpha}=1$. The Lagrange optimization of the constrained ML expression
\begin{equation}
\sum_s{\sum_\alpha{\log{(t_\alpha)}C_\alpha(s,\pi_s)}} - \lambda\sum_\alpha{t_\alpha}
\end{equation}
for a Lagrange multiplier $\lambda$ has the close form solution
\begin{equation}
t_\alpha^{*} = \frac{\sum_s{C_\alpha(s,\pi_s)}}{\sum_s{\sum_\beta{C_\beta(s,\pi_s)}}}
\end{equation}
Eventually, the ML estimation of probabilistic parameters corresponds to taking the total frequency of occurrence of features in the training set. 
It is important to highlight that this method alone is problematic when the data are scarce, because some parameter will get a zero value, thus overfitting to the data, due to this fact it becomes critically important the selection of a proper training set.

\subsection{Folding Algorithm}
\label{sec:6}
Most physics-based approached to secondary structure prediction use dynamic programming to recover the structure with minimum free energy \cite{Zuker:2003}. 
For probabilistic methods, the Cocke-Younger-Kasami (known as the CYK algorithm for SCFGs \cite{Durbin:1998,Hofacker:1994}), an RNA secondary structure ad hoc version of the Viterbi algorithm, fulfills this function by finding the most likely parse, that is, for unambiguous grammars, also the most likely secondary structure. 
However, this is not the case for ambiguous grammars \cite{Dowell:2004,Reeder:2005}.
\begin{equation}
\hat{\sigma}_{viterbi}=arg max [P(\hat{\sigma}|x;w)]
\end{equation}
Here it is described an alternative scheme, first proposed by CONTRAfold \cite{Do:2006} that, for a given setting of sensitivity/specificity  tradeoff parameter $\gamma$, identifies the structure with maximum expected accuracy (mea).
In particular, for a candidate structure $\hat{ss}$ and its real structure $ss$ we define $accuracy_{\gamma}(\hat{ss},ss)$ denoting the number of correctly unpaired positions in $\hat{ss}$ (with respect to $ss$) plus $\gamma$ times the number of correctly paired positions in $\hat{ss}$.
Given this measure the goal is to retrieve the most accurate among all the possible structures.
\begin{equation}
\hat{ss}_{mea}=arg max \mathbb{E}_{ss}[accuracy_{\gamma}(\hat{ss},ss)]
\end{equation}
In equation 2.6 the expectation is taken with respect to the conditional distribution over possible structures of the target sequence. 
The algorithm used to calculate this quantity follows a dynamic programming approach.
Denoting with $p_{ij}$ the conditional probability of nucleotides $i$th and $j$th to be paired.
Similarly, let $q_i=1-\sum_j{p_{ij}}$ be the conditional probability that nucleotide $i$th is unpaired. 
The following recurrence computes 
$M_{1,L}=max_ss(\mathbb{E}_{ss}[accuracy_{\gamma}(\hat{ss}_{mea},ss)])$ where $L$ is the length of the sequence.
\begin{equation}
 M_{i,j} = max 
  \begin{cases}
    q_i  &  \text{if $i=j$},  \\
    q_i+M_{i+1,j}  &  \text{if $i<j$},  \\
    q_j+M_{i,j-1}  &  \text{if $i<j$},  \\
    \gamma\cdot2p_{ij}+M_{i+1,j-1}  &  \text{if $i+2\leq{j}$},  \\
    M_{i,k}+M_{k+1,j}  &  \text{if $i\leq{k}<j$}.
  \end{cases}  
\end{equation}
Including the traceback to recover the optimal structure, the parsing algorithm takes $O(L^3)$ time and $O(L^2)$ space.
Note that in the above algorithm, $\gamma$ controls the balance between the sensitivity and specificity of the returned structure, therefore, higher values of $\gamma$ encourage the parser to predict more base pairings, whereas lower values of $\gamma$ restrict the parse to predict only base pairs for which the algorithm is extremely confident.
When $\gamma=1$, the algorithm maximizes the expected number of correct positions and is identical to the parsing technique used in Pfold \cite{Knudsen:2003}.
\section{GRNApred}
\label{sec:7}
This section describes the development and the validation process of a grammar-based RNA predictors ranker (GRNApred).
GRNApred is a computational tool capable of automatically select the best grammar and training dataset starting from a set of sequence derived features. 
First, twenty one grammars from the literature have been chosen and three sequence derived features with relevant biological meaning for RNA molecules have been used to divide the original dataset. 
The training has the goal of differently parameterize each one of the grammars with different set of data. 
The comparison of the grammars, calculated by a score specific score function, permits to build a ranking.
This ranking will be used in the testing procedure to predict the target sequence.
\subsection{Training Procedure}
\label{sec:8}
During the training procedure a set of sequence-derived features are selected and the original datasets is divided into subsets that contains data from the same range of values for the selected feature. 
Each one of the built dataset is then used to perform a cross validation of the grammars, from which an accuracy measure is retrieved. 
Algorithms \ref{alg:1} shows the pseudo-code for training procedure.
This accuracy is used to score each grammar for the correspondent dataset. 
At the end of the procedure each one of the subsets goes with a ranking of grammars, as shown in figure \ref{fig:2} at the end of the document.
\subsection{Testing Procedure}
\label{sec:9}
Given a target RNA sequence the best model is automatically selected according to the features derived from the target.
These models are used to retrieve a set of possible RNA structures predicted with high confidence. 
The resultant structures are in Stockholm formatted file.
The model comes with the best pair of grammar and train set for each feature. 
Algorithms \ref{alg:2} shows the pseudo-code for testing procedure, figure \ref{fig:3} shows the steps of the testing procedure. 
A target file in Stockholm format is passed to the predict function of GRNApred, features from the sequence are calculated and the model selected, finally the prediction file is produced in the desired output.
\section{Evaluation Measures}
\label{sec:10}
Evaluation measures for the assessment of an RNA secondary structure prediction accuracy extensively used in the literature, are the sensitivity and the positive predicted value.
Sensitivity (SEN) also called the true positive rate, or the recall rate measures the proportion of actual positives which are correctly identified as such ($\mathbb{TPR} = \frac{\mathbb{TP}}{\mathbb{P}} = \frac{\mathbb{TP}}{(\mathbb{TP}+\mathbb{FN})}$).
Positive predicted value(PPV) also called precision, describes the performance of the predictor, in this case base pairs, measuring the actual proportion between the correctly predicted values and the totality of the predictions on the target condition ($\mathbb{PPV} = \frac{\mathbb{TP}}{(\mathbb{TP}+\mathbb{FP})}$).
\begin{equation}
  \begin{split}
  SEN = \frac{Number\ of\ True\ Pairs\ Predicted}{Number\ of\ True\ Pairs} \\
  PPV = \frac{Number\ of\ True\ Pairs\ Predicted}{Number\ of\ Pairs\ Predicted}
  \end{split}
\end{equation}
Oftentimes, for simplicity, it is used the F measure \cite{Rijsbergen:1979} (or F1 measure) that identifies the harmonic mean of sensitivity and PPV as a proxy for prediction accuracy.
\begin{equation}
  F = \frac{1}{2}\frac{1}{\frac{1}{SEN}+\frac{1}{PPV}}
\end{equation}