/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.github.stefanopiozingaro.ragnarok.scfgrna;

/**
 *
 * @author Gualberto
 */
public class DatasetStatistics {
	
	private float averageLength;
	private int size;
	private int minimumLength;
	private int maximumLength;
	private float stdevLength;

	public DatasetStatistics(float averageLength, int size, int minimumLength, int maximumLength, float stdevLength) {
		this.averageLength = averageLength;
		this.size = size;
		this.minimumLength = minimumLength;
		this.maximumLength = maximumLength;
		this.stdevLength = stdevLength;
	}

	public float getAverageLength() {
		return averageLength;
	}

	public void setAverageLength(float averageLength) {
		this.averageLength = averageLength;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getMinimumLength() {
		return minimumLength;
	}

	public void setMinimumLength(int minimumLength) {
		this.minimumLength = minimumLength;
	}

	public int getMaximumLength() {
		return maximumLength;
	}

	public void setMaximumLength(int maximumLength) {
		this.maximumLength = maximumLength;
	}

	public float getStdevLength() {
		return stdevLength;
	}

	public void setStdevLength(float stdevLength) {
		this.stdevLength = stdevLength;
	}

	@Override
	public String toString() {
		return "DatasetStatistics{" + "averageLength=" + averageLength + ", size=" + size + ", minimumLength=" + minimumLength + ", maximumLength=" + maximumLength + ", stdevLength=" + stdevLength + '}';
	}
	
}
