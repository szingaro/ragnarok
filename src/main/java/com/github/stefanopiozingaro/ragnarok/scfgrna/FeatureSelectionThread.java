/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CorrelationAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author Gualberto
 */
public class FeatureSelectionThread extends Thread {
	
	public static HashMap<String, SortedSet<PredictionPerformance>> ranking = new HashMap<String, SortedSet<PredictionPerformance>> ();
	private String arffFile;
	private String dataset; // name of the input dataset
	private TrainTestGrammarThread[][] warehouse; // the threads which contain execution times of each grammar and fold (for one input data file)
	

	public FeatureSelectionThread(String arffFile, TrainTestGrammarThread[][] warehouse) {
		this.arffFile = arffFile;
		this.warehouse = warehouse;
		dataset = arffFile.substring(arffFile.lastIndexOf(File.separator)+1, arffFile.lastIndexOf('.'));
	}
	
	private double [][] calculateAccuracies () {
		if (GRNApred.rankingGrammarsCriteria == GRNApred.RANKING_LINEAR_CORR)
			return calculateAccuraciesLinearCorr();
		else if (GRNApred.rankingGrammarsCriteria == GRNApred.RANKING_2F1_1EXECTIME)
			return calculateAccuraciesF1();
		else 
			return null;
	}

	private double [][] calculateAccuraciesF1 () {
		double [][] res = null;
		
		DataSource source;
		try {
			source = new DataSource(arffFile);
			Instances data = source.getDataSet();
			data.setClassIndex(data.numAttributes() - 1);
			Iterator<Instance> it = data.iterator();
			Instance x; int i = 0, nattrs;
			int [] tp, tn, fp, fn;
			double ppv, sens; // ppv = accuracy = precision // sens = recall = coverage

			nattrs = data.numAttributes() - 4;
			res = new double[nattrs][4]; // Header of 3 attributes + class
			tp = new int[nattrs];
			tn = new int[nattrs];
			fp = new int[nattrs];
			fn = new int[nattrs];
			
			for (i = 0; i < res.length; i++) {
				res[i][0] = i;
			}
			
			// iterate all instances in arff and calculate tp,tn,fp,fn of all grammars (attributes: 3..#class-1)
			while (it.hasNext()) {
				x = it.next();
				for (i = 0; i < res.length; i++) {
					if (!x.isMissing(i + 3)) { // Header of 3 attributes + class
						if (x.value(i + 3) > 0.5) { // predicted contact
							if (x.classValue() == 1.0) // real contact
								tp[i]++;
							else
								fp[i]++;
						}
						if (x.value(i + 3) <= 0.5) { // predicted non-contact
							if (x.classValue() == 1.0) // real contact
								fn[i]++;
							else
								tn[i]++;
						}
					}
				}
			}
			
			for (i = 0; i < res.length; i++) {
				ppv = ((double)tp[i]) / (tp[i] + fp[i]);
				sens = ((double)tp[i]) / (tp[i] + fn[i]);
				res[i][1] = 2 * ppv * sens / (ppv + sens);
				res[i][2] = ppv;
				res[i][3] = sens;
			}
			
		} catch (Exception ex) {
			System.out.println("Exception: " + ex);
			return null;
		}		
		
		return res;
	}

	private double [][] calculateAccuraciesLinearCorr () {
		try {
			DataSource source = new DataSource(arffFile);
			Instances data = source.getDataSet();
			Remove filter = new Remove();
			filter.setAttributeIndices("1,2,3");  // remove attributes corresponding to the header (seqID, i and j)
			filter.setInputFormat(data);
			data = Filter.useFilter(data, filter);
			data.setClassIndex(data.numAttributes() - 1);
			AttributeSelection attsel = new AttributeSelection();  // package weka.attributeSelection!
	
			// the method to perform feature selection
			CorrelationAttributeEval eval = new CorrelationAttributeEval();
			Ranker search = new Ranker();
	
			// perform the feature selection (ranking)
			attsel.setEvaluator(eval);
			attsel.setSearch(search);
			attsel.SelectAttributes(data);
			return attsel.rankedAttributes();
		} catch (Exception ex) {
			Logger.getLogger(FeatureSelectionThread.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}
	

	@Override
	public void run() {
		
		double [][] accuracies = calculateAccuracies();
		// System.out.println(Utils.arrayToString(accuracies));
			
		// integrate obtained accuracies with execution times and sort with criteria: ( 2 * HarmMean(PPV,Sens) + ExecTime ) / 3
		SortedSet<PredictionPerformance> performances = new TreeSet<PredictionPerformance>();
		PredictionPerformance pp;
		long [] execTime = new long[accuracies.length];
		long minExecTime, maxExecTime;
		
		// calculate minimum and maximum values of both score in ranked feature selection and execution times //
		minExecTime = Long.MAX_VALUE; maxExecTime = 0; 
		for (int i = 0; i < accuracies.length; i++) { // for each grammar (attribute)
			execTime[i] = 0;
			for (int f = 0; f < warehouse.length; f++) { // for each fold (the first dimension of the matrix warehouse contains the folds of the current dataset)
				// sum of training and test times of each fold for the grammar given by output[i]
				execTime[i] += warehouse[f][(int)accuracies[i][0]].getTrainingTime() + warehouse[f][(int)accuracies[i][0]].getTestingTime();
			}
			if (execTime[i] < minExecTime)
				minExecTime = execTime[i];
			if (execTime[i] > maxExecTime)
				maxExecTime = execTime[i];
		}
		
		// normalize both scores and times and store in the sorted set performances //
		for (int i = 0; i < accuracies.length; i++) { // for each grammar (attribute)
			pp = new PredictionPerformance(GRNApred.grammars[(int)accuracies[i][0]], warehouse[0][0].getTrainingName(), accuracies[i][1], execTime[i], 
					((double)execTime[i] - minExecTime) / (maxExecTime - minExecTime),
					accuracies[0].length > 2 ? accuracies[i][2] : -1.0, accuracies[0].length > 3 ? accuracies[i][3] : -1.0);
			performances.add(pp);
		}
		ranking.put(dataset, performances);
					
		// additional task: consolidate .param/.counts knowledge of folds into one //
		// consolidate();  // it does not work, grm-parse produces an internal error freeing some memory, my parameters are correct --> no problem, it is possible to consolidate in testing time with grm-fold passing several .counts //
		
	}
	
	private void consolidate () {
		String [] files;
		String cmd;
		
		for (int g = 0; g < GRNApred.grammars.length; g++) { // for each grammar //
			// collect .counts files for the grammar from given dataset //
			final int gg = g; // artificious //
			files = (new File(GRNApred.KNOWLEDGE_FOLDER)).list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.startsWith(dataset) && name.endsWith(GRNApred.grammars[gg] + ".param");
				}
			});
			cmd = "tornado" + File.separator + "src" + File.separator + "grm-parse --lprob --paramsavefile " + GRNApred.KNOWLEDGE_FOLDER + File.separator +
				  dataset + "_" + GRNApred.grammars[g] + ".param " + GRNApred.GRAMMARS_FOLDER + File.separator + GRNApred.grammars[g] + ".grm";
			for (String file : files) {
				cmd += " " + GRNApred.KNOWLEDGE_FOLDER + File.separator + file;
			}
			// System.out.println(cmd);
			TrainTestGrammarThread.runCommand(cmd, null, 0, null, null);
		}
	}
	
}
