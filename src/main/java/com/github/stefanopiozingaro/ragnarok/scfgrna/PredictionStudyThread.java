package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PredictionStudyThread extends Thread {
	
	private PredictionStudy study;
	private int trainIdx, predictorIdx;
	private boolean isOurPredictor;
	private String cmdLine;
	private PredictionPerformance [] performances;

	private static Semaphore testingSemaphore = new Semaphore(GRNApred.maxTestingGrammarsInExecution);
	
	
	public PredictionStudyThread(PredictionStudy study, int generalIdx) {
		super();
		this.study = study;
		if (generalIdx < study.getNumTrainings() * study.getNumOurPredictors()) {
			isOurPredictor = true;
			trainIdx = generalIdx / study.getNumOurPredictors();
			predictorIdx = generalIdx % study.getNumOurPredictors();
			cmdLine = study.getOurPredictionPerformance(trainIdx, predictorIdx, 0).getCmdLineTesting(); 
			performances = study.getOurPredictionPerformance(trainIdx, predictorIdx);
			//System.out.println(generalIdx + ": " + isOurPredictor + " " + trainIdx + " " + predictorIdx + " " + cmdLine);
		}
		else {
			isOurPredictor = false;
			predictorIdx = generalIdx - study.getNumTrainings() * study.getNumOurPredictors();
			cmdLine = study.getExtPredictionPerformance(predictorIdx, 0).getCmdLineTesting();
			performances = study.getExtPredictionPerformance(predictorIdx);
			//System.out.println(generalIdx + ": " + isOurPredictor + " " + predictorIdx + " " + cmdLine);
		}
	}

	@Override
	public void run() {
		super.run();
		long testingTime;
		StringBuffer output = new StringBuffer("");
		try {
			testingSemaphore.acquire();
		} catch (InterruptedException ex) { Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex); }
		testingTime = TrainTestGrammarThread.runCommand(cmdLine, "F = ", study.getNumTargets(), "%%% - Predicting target", output);
		testingSemaphore.release();
		extractPredictionResults(output.toString(), testingTime);
		// System.out.println(output);
	}

	private void extractPredictionResults(String output, long execTime) {
		int i = 0, pos;
		
		pos = output.indexOf("sen = ");
		while (pos != -1) {
			performances[i].setSens(Double.parseDouble(output.substring(pos + 6, output.indexOf(" ", pos + 6))) / 100);
			pos = output.indexOf("ppv = ", pos);
			performances[i].setPpv(Double.parseDouble(output.substring(pos + 6, output.indexOf(" ", pos + 6))) / 100);
			pos = output.indexOf("F = ", pos);
			performances[i].setAccuracy(Double.parseDouble(output.substring(pos + 4, output.indexOf("\n", pos + 4))) / 100);
			performances[i].setExecTime(execTime);
			pos = output.indexOf("sen = ", pos);
			i++;
		}		
	}
	
}
