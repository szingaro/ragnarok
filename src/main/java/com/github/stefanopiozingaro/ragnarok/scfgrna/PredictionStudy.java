package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Requirements for knowledge file:
 * 
 * - First column must be the name of the training dataset
 * - Second column must be the type of the training dataset ("byLength", "byGCcontent", "bySeqType")
 * - Last column must be the ranking of grammars with decimal points by '.' in numbers
 * 
 * @author Gualberto
 *
 */
public class PredictionStudy {
	
	private String targetsSto, knowledgeFilename;
	private String [] targets;
	private int [] lengths;
	private float [] gcContents;
	private PredictionPerformance [][][] ourPredictions;
	private PredictionPerformance [][] externalPredictions;
	private PredictionPerformance [][][] differences;
	private String [] trainingNames;
	private String [] trainingTypes;
	private String [][] ourPredictorNames;
	private String [] externalPredictorNames;
	private String [] differenceCriterias = {"byLength","byGCcontent"};
	private int [][][] bestPredictionsIdxByTarget;
	
	private static String [] colorsDifferenceCriterias = {"orange", "green","red"}; // the last color means best predictor without criterias
	
	
	public PredictionStudy (String targetsSto, String knowledgeFilename, String [] externalPredictors, int topOurPredictors) {
		List<String> sequences;
		Iterator<String> it;
		String seq, line; String [] aux;
		int i, j, k, ntargets, ntrains, nourpredictors;
		
		// INITIALIZE TARGETS INFORMATION //
		this.targetsSto = targetsSto;
		this.knowledgeFilename = knowledgeFilename;
		sequences = GRNApred.readSequencesFromSTO(targetsSto);
		it = sequences.iterator();
		ntargets = sequences.size(); i = 0;
		targets = new String [ntargets];
		lengths = new int [ntargets];
		gcContents = new float [ntargets];
		while (it.hasNext()) {
			seq = it.next();
			aux = GRNApred.parseSequence(seq);
			targets[i] = aux[0];
			lengths[i] = aux[1].length();
			gcContents[i] = calcGCcontent(aux[1]); 
			i++;
		}

		// INITIALIZE STRUCTURE FOR PREDICTIONS //
		try {
			// First read: count number of training and dimensioning //
			BufferedReader km = new BufferedReader(new FileReader(knowledgeFilename));
			line = km.readLine(); // skipping the header row //
			line = km.readLine();
			ntrains = 0; nourpredictors = 0;
			while (line != null) {
				if (nourpredictors == 0) {
					aux = line.split("\t");
					nourpredictors = aux[aux.length-1].split(",").length;
				}
				ntrains++;
				line = km.readLine();
			}			
			km.close();			
			ourPredictions = new PredictionPerformance[ntrains][topOurPredictors][ntargets];
			differences = new PredictionPerformance[externalPredictors.length][differenceCriterias.length][ntargets];
			trainingNames = new String [ntrains];
			trainingTypes = new String [ntrains];
			ourPredictorNames = new String [ntrains][topOurPredictors];
			bestPredictionsIdxByTarget = new int [ntargets][differenceCriterias.length + 1][2];
			
			// Second read: extract knowledge //
			km = new BufferedReader(new FileReader(knowledgeFilename));
			line = km.readLine(); // skipping the header row //
			line = km.readLine();
			i = 0;
			while (line != null) {
				aux = line.split("\t");
				trainingNames[i] = aux[0];
				trainingTypes[i] = aux[1];
				aux = aux[aux.length-1].substring(1, aux[aux.length-1].length()-1).split(", ");
				for (j = 0; j < topOurPredictors; j++) {
					if (aux[j].indexOf('[') >= 0)
						ourPredictorNames[i][j] = aux[j].substring(0, aux[j].indexOf('['));
					else
						ourPredictorNames[i][j] = aux[j].substring(0, aux[j].indexOf('('));
					for (k = 0; k < ntargets; k++) {
						ourPredictions[i][j][k] = new PredictionPerformance(ourPredictorNames[i][j], trainingNames[i]);
					}
					
					// Note.- Only the first object in ourPredictions[i][j] will contain the command line (because prediction are in batch in that command). // 
					ourPredictions[i][j][0].setCmdLineTesting(genCmdLineTesting(ourPredictorNames[i][j], trainingNames[i], targetsSto, 
							GRNApred.PREDICTION_FOLDER + File.separator + targetsSto.substring(targetsSto.lastIndexOf(File.separator) + 1)
							.replace(".sto", "_predictedby-" + ourPredictorNames[i][j] + "_trainedwith-" + trainingNames[i] + ".sto")));
					//System.out.println("DEF: " + ourPredictions[i][j][0].getCmdLineTesting());
				}
				line = km.readLine();
				i++;
			}			
			km.close();
			
			// Initialize external predictors //
			externalPredictions = new PredictionPerformance[externalPredictors.length][ntargets];
			externalPredictorNames = externalPredictors;
			for (i = 0; i < externalPredictors.length; i++) {
				for (j = 0; j < ntargets; j++) {
					externalPredictions[i][j] = new PredictionPerformance(externalPredictorNames[i], "-");
				}
				
				// Note.- Only the first object in ourPredictions[i][j] will contain the command line (because prediction are in batch in that command). // 
				externalPredictions[i][0].setCmdLineTesting(genCmdLineExternalPredictor(externalPredictorNames[i], targetsSto, 
						GRNApred.PREDICTION_FOLDER + File.separator + targetsSto.substring(targetsSto.lastIndexOf(File.separator) + 1)
						.replace(".sto", "_predictedbyExternal-" + externalPredictorNames[i] + ".sto")));
				//System.out.println("DEF: " + externalPredictions[i][0].getCmdLineTesting());
				
				for (j = 0; j < differenceCriterias.length; j++) {
					for (k = 0; k < ntargets; k++) {
						differences[i][j][k] = new PredictionPerformance("Diff(" + externalPredictors[i] + "," + differenceCriterias[j] + ")", "*");
					}					
				}
			}

		} catch (IOException ex) {
			Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
		}	
	}
	
	public int getNumTargets () {
		return targets.length;
	}
	
	public int getNumTrainings () {
		return ourPredictions.length;
	}
	
	public int getNumOurPredictors () {
		return ourPredictions[0].length;
	}
	
	public int getNumExtPredictors () {
		return externalPredictions.length;
	}
	
	public int getNumPredictionsOverAllTargets () {
		return ourPredictions.length * ourPredictions[0].length + externalPredictions.length;
	}
	
	public PredictionPerformance [] getOurPredictionPerformance (int trainIdx, int predictorIdx) {
		return ourPredictions[trainIdx][predictorIdx];
	}

	public PredictionPerformance [] getExtPredictionPerformance (int predictorIdx) {
		return externalPredictions[predictorIdx];
	}

	public PredictionPerformance getOurPredictionPerformance (int trainIdx, int predictorIdx, int targetIdx) {
		return ourPredictions[trainIdx][predictorIdx][targetIdx];
	}

	public PredictionPerformance getExtPredictionPerformance (int predictorIdx, int targetIdx) {
		return externalPredictions[predictorIdx][targetIdx];
	}

	public void produceHTML (String outputFilename) {
		
		int i, j, k, l;
		String color, output = "<html><head>GRNApred - Prediction Study (" + new Date().toString() + ")<br>Gualberto Asencio Cortes, PhD (2014)</head><body><br><br><br>";
		
		output += "<EM>[Target STO file: " + this.targetsSto + "] [Knowledge file: " + this.knowledgeFilename + "]</EM><TABLE border='1' CELLPADDING='5'>\r\n";
		output += "<TR><TH rowspan='2'>Target<TH rowspan='2'>Length<TH rowspan='2'>GC-content";
		
		for (i = 0; i < trainingNames.length; i++) {
			output += "<TH colspan='" + ourPredictorNames[i].length + "'>" + trainingNames[i] + "\r\n";			
		}
		
		for (i = 0; i < externalPredictorNames.length; i++) {
			output += "<TH rowspan='2'>" + externalPredictorNames[i] + "\r\n";			
		}
		
		for (i = 0; i < externalPredictorNames.length; i++) {
			for (j = 0; j < differenceCriterias.length; j++) {
				output += "<TH rowspan='2'>Diff(" + externalPredictorNames[i] + "," + differenceCriterias[j] + ")\r\n";
			}
		}
		
		output += "<TR>";
		for (i = 0; i < ourPredictorNames.length; i++) {
			for (j = 0; j < ourPredictorNames[i].length; j++) {
				output += "<TH>" + ourPredictorNames[i][j];
			}
		}		
		output += "\r\n";
		
		calcBestPredictionsIdxByTarget();
		
		for (i = 0; i < targets.length; i++) {
			output += "<TR><TD>" + targets[i] + "<TD>" + lengths[i] + "<TD>" + GRNApred.formatter.format(gcContents[i]).replace(',','.');
			for (j = 0; j < ourPredictorNames.length; j++) {
				for (k = 0; k < ourPredictorNames[j].length; k++) {
					color = "white";
					if (bestPredictionsIdxByTarget[i][differenceCriterias.length][0] == j && bestPredictionsIdxByTarget[i][differenceCriterias.length][1] == k)
						color = colorsDifferenceCriterias[differenceCriterias.length];
					//for (l = 0; l < differenceCriterias.length; l++) {
					//	if (bestPredictionsIdxByTarget[i][l][0] == j && bestPredictionsIdxByTarget[i][l][1] == k)
					//		color = colorsDifferenceCriterias[l];
					//}
					output += "<TD bgcolor='" + color + "'>" + ourPredictions[j][k][i].abbreviateToString();
				}
			}
			for (j = 0; j < externalPredictions.length; j++) {
				output += "<TD>" + externalPredictions[j][i].abbreviateToString();			
			}
			for (j = 0; j < differences.length; j++) {
				for (k = 0; k < differences[j].length; k++) {
					output += "<TD>" + differences[j][k][i].abbreviateToString();
				}
			}
			output += "\r\n";
		}

		output += "</TABLE>\r\n";
		
		output += "</body></html>";
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFilename));
			bw.write(output);								
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void calcBestPredictionsIdxByTarget() {
		int i, j, k, l;
		float x = 0;
		double max;
		boolean found;

		for (i = 0; i < targets.length; i++) {
			// find selected our predictor depending on the different criterias //
			for (j = 0; j < differenceCriterias.length; j++) {
				found = false; k = 0;
				while ( k < trainingNames.length && !found ) {
					if (differenceCriterias[j].equals("byLength"))
						x = lengths[i];
					else if (differenceCriterias[j].equals("byGCcontent"))
						x = gcContents[i];
					if (trainingTypes[k].equals(differenceCriterias[j]) && checkInterval(x, trainingNames[k])) {
						bestPredictionsIdxByTarget[i][j][0] = k;
						bestPredictionsIdxByTarget[i][j][1] = 0; // always the first best grammar of this training
						found = true;
					}
					k++;
				}
			}
			// find best predictor for target i //
			max = 0;
			for (k = 0; k < ourPredictions.length; k++) {
				for (l = 0; l < ourPredictions[k].length; l++) {
					if (ourPredictions[k][l][i].getAccuracy() > max) { 
						bestPredictionsIdxByTarget[i][differenceCriterias.length][0] = k;
						bestPredictionsIdxByTarget[i][differenceCriterias.length][1] = l;
						max = ourPredictions[k][l][i].getAccuracy();
					}
				}
			}			
		}		
	}

	/**
	 * The names of trainings with type 'byLength' must end like "_100-200", indicating an interval of length.
	 * 
	 * @param i
	 * @param name
	 * @return
	 */
	private boolean checkInterval(float i, String name) {
		int a, b;
		String [] aux;
		
		aux = name.substring(name.lastIndexOf("_") + 1).split("-");
		a = Integer.parseInt(aux[0]);
		b = Integer.parseInt(aux[1]);
		
		return a <= i && i <= b;
	}

	private String genCmdLineExternalPredictor(String predictorName, String targetsSto, String output) {
		String cmd = "";
		
		if (predictorName.equals("VRNA")) {
			cmd = TrainTestGrammarThread.cmdTestGrammar + " " + GRNApred.GRAMMARS_FOLDER + File.separator + "ViennaRNAG.grm " + targetsSto + " " + output;						
		}
		
		return cmd;
	}

	private String genCmdLineTesting(final String grammar, final String trainDataset, String target, String output) {
		String cmd;
		
		cmd = TrainTestGrammarThread.cmdTestGrammar + " --auc --count " + GRNApred.GRAMMARS_FOLDER + File.separator + grammar + ".grm " + target + " " + output;			
		String [] counts = (new File(GRNApred.KNOWLEDGE_FOLDER)).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(trainDataset) && name.endsWith(grammar + ".counts");
			}
		});
		for (String file : counts) {
			cmd += " " + GRNApred.KNOWLEDGE_FOLDER + File.separator + file;
		}	
			
		return cmd;
	}

	private float calcGCcontent(String sequence) {
		int count = 0;
		int i, n = sequence.length();
		
		for (i = 0; i < n; i++) {
			if (sequence.charAt(i) == 'C' || sequence.charAt(i) == 'G')
				count++;
		}
		
		return ((float)count) / n;
	}
}
