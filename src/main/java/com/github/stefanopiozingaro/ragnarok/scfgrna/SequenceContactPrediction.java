package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Class SequenceContactPrediction: Consensus of stochastic context free grammars to RNA secondary structure prediction.
 * 
 * @author Gualberto Asencio Cortes [Bologna, Jule 9 2014]
 */
public class SequenceContactPrediction {
	
	// private HashMap<Pair,Attributes> data;
	private float [][][] predictions;
	private int numGrammarsInserted; // current number of grammars inserted (last dimension of the array) (in order to detect when all are inserted)
	private int length; // the length of the sequence
	private String secondaryStructure;

	public SequenceContactPrediction (int length, String ss) {
		predictions = new float[length][length][GRNApred.grammars.length]; //data = new HashMap<Pair,Attributes>(1 + length * (length - 1) / 2); // initial capacity for all possible pairs of monomers
		numGrammarsInserted = 0;
		this.length = length;
		secondaryStructure = ss;
	}
	
	public SequenceContactPrediction (int length) {
		this.length = length;
	}
	
	public void putPrediction(int i, int j, String grammar, float value) {
		//if (value != 0)
		//	System.out.println(i + " " + j + " " + GRNApred.grammarsIndices.get(grammar) + " = " + value);
		predictions[i][j][GRNApred.grammarsIndices.get(grammar)] = value;
	}
	
	public boolean increaseGrammarsInserted () {
		numGrammarsInserted++;
		if (numGrammarsInserted == GRNApred.grammars.length)
			return true;
		else
			return false;
	}

	public void storeInARFF(String seqID) {
		int i, j, g, ng = GRNApred.grammars.length;
		String temp;
		float[][] contacts = new float[length][length];
		TrainTestGrammarThread.addContacts(secondaryStructure, contacts);
		
		for (i = 0; i < length; i++) {
			for (j = 0; j < length; j++) {
				if (j - i > GRNApred.minSep) {
					temp = seqID + "," + i + "," + j;
					for (g = 0; g < ng; g++) {
						temp += "," + (Float.isNaN(predictions[i][j][g]) ? "?" : predictions[i][j][g]);
					}
					temp += "," + (contacts[i][j] == 0 ? "0" : "1");
					try {
						GRNApred.arff.write(temp + "\r\n");
					} catch (IOException ex) {
						Logger.getLogger(SequenceContactPrediction.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}
	
	public int getLength () {
		return length;
	}

}
