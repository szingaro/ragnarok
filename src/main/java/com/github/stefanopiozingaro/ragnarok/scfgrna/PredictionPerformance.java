/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.github.stefanopiozingaro.ragnarok.scfgrna;

/**
 *
 * @author Gualberto
 */
public class PredictionPerformance implements Comparable {
	
	private String predictorName, learnedTraining, cmdLineTesting;
	private double accuracy, ppv, sens;
	private long execTime;
	private double execTimeNormalized;

	public PredictionPerformance(String predictorName, String learnedTraining, double accuracy, long execTime, double execTimeNormalized, double ppv, double sens) {
		this.predictorName = predictorName;
		this.learnedTraining = learnedTraining;
		this.accuracy = accuracy;
		this.execTime = execTime;
		this.execTimeNormalized = execTimeNormalized;
		this.ppv = ppv;
		this.sens = sens;
	}

	public PredictionPerformance(String predictorName, String learnedTraining) {
		this.predictorName = predictorName;
		this.learnedTraining = learnedTraining;
	}
	
	public String getCmdLineTesting() {
		return cmdLineTesting;
	}
	
	public void setCmdLineTesting(String cmdLineTesting) {
		this.cmdLineTesting = cmdLineTesting;
	}

	public String getPredictorName() {
		return predictorName;
	}

	public void setPredictorName(String predictorName) {
		this.predictorName = predictorName;
	}

	public double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(float accuracy) {
		this.accuracy = accuracy;
	}

	public long getExecTime() {
		return execTime;
	}

	public void setExecTime(long execTime) {
		this.execTime = execTime;
	}

	public String getLearnedTraining() {
		return learnedTraining;
	}

	public void setLearnedTraining(String learnedTraining) {
		this.learnedTraining = learnedTraining;
	}

	public double getPpv() {
		return ppv;
	}

	public void setPpv(double ppv) {
		this.ppv = ppv;
	}

	public double getSens() {
		return sens;
	}

	public void setSens(double sens) {
		this.sens = sens;
	}

	public double getExecTimeNormalized() {
		return execTimeNormalized;
	}

	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}

	@Override
	public int compareTo(Object o) {
		int res = 0;
		PredictionPerformance pp = (PredictionPerformance)o;
		double x1, x2;
		
		x1 = (2 * accuracy + (1 - execTimeNormalized)) / 3;
		x2 = (2 * pp.accuracy + (1 - pp.execTimeNormalized)) / 3;		
		
		if (x1 > x2)
			res = -1;
		else if (x1 < x2)
			res = 1;
		
		return res;
	}

	@Override
	public String toString () {
		return predictorName + "[" + learnedTraining + "](Acc=" + GRNApred.formatter.format(accuracy).replace(',','.') + "|PPV=" + GRNApred.formatter.format(ppv).replace(',','.') + "|Sens=" + GRNApred.formatter.format(sens).replace(',','.') + "|Time=" + execTime + "|TimeNorm=" + GRNApred.formatter.format(execTimeNormalized).replace(',','.') + "|score=" + GRNApred.formatter.format((2 * accuracy + (1 - execTimeNormalized)) / 3).replace(',','.') + ")";
	}

	public String abbreviateToString () {
		return "Acc=" + GRNApred.formatter.format(accuracy).replace(',','.') + "|PPV=" + GRNApred.formatter.format(ppv).replace(',','.') + "|Sens=" + GRNApred.formatter.format(sens).replace(',','.') + "|Time=" + execTime;
	}
}
