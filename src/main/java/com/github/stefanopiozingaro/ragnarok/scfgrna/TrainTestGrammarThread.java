package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class TrainTestGrammarThread: Thread of Consensus of stochastic context free grammars to RNA secondary structure prediction.
 * 
 * @author Gualberto Asencio Cortes [Bologna, Jule 9 2014]
 */
class TrainTestGrammarThread extends Thread {
		
	private static Semaphore trainingSemaphore = new Semaphore(GRNApred.maxTrainingGrammarsInExecution);
	private static Semaphore testingSemaphore = new Semaphore(GRNApred.maxTestingGrammarsInExecution);
	public static HashMap<String,SequenceContactPrediction> output;
	
	private static final String cmdTrainGrammar = "tornado" + File.separator + "src" + File.separator + "grm-train";
	public static final String cmdTestGrammar = "tornado" + File.separator + "src" + File.separator + "grm-fold";
	
	private String training, test, grammar, nameTraining, nameTest, nameGrammar;
	private long trainingTime, testingTime, computeStoreTime;
	private String grammarParams, grammarCounts; // names of both param and counts files once grammar is trained //
	

	public TrainTestGrammarThread (String [] trainTestFilenames, String grammarFile) {
		training = trainTestFilenames[0];
		test = trainTestFilenames[1];
		grammar = grammarFile;
		nameTraining = training.substring(training.lastIndexOf(File.separator)+1, training.lastIndexOf('.'));
		nameTest = test.substring(test.lastIndexOf(File.separator)+1, test.lastIndexOf('.'));
		nameGrammar = grammar.substring(grammar.lastIndexOf(File.separator)+1, grammar.lastIndexOf('.'));
	}
	
	public String getTrainingName () {
		return nameTraining;
	}

	@Override
	public void run() {
		// train the grammar //
		try {
			trainingSemaphore.acquire();
		} catch (InterruptedException ex) { Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex); }
		trainingTime = trainGrammar();
		trainingSemaphore.release();
		System.out.println("111 - Training of " + nameTraining + " with grammar " + nameGrammar + " is finished (" + (trainingTime / 60000.0) + " mins (" + (trainingTime / 1000.0) + " secs).");
		
		// test the grammar //
		try {
			testingSemaphore.acquire();
		} catch (InterruptedException ex) { Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex); }
		testingTime = testGrammar();
		testingSemaphore.release();
		System.out.println("222 - Testing of " + nameTest + " with grammar " + nameGrammar + " is finished (" + (testingTime / 60000.0) + " mins (" + (testingTime / 1000.0) + " secs).");
		
		// compute and store (adding to the arff file, if proceed) all predicted contacts of the sequences in the test //
		computeStoreTime = computeAndStoreResults();
		System.out.println("333 - Computing and storing of " + nameTraining + "/" + nameTest + " with grammar " + nameGrammar + " is finished (" + (computeStoreTime / 60000.0) + " mins (" + (computeStoreTime / 1000.0) + " secs).");
	}

	private long trainGrammar() {
		String cmd = cmdTrainGrammar + " -v --countsavefile " + GRNApred.KNOWLEDGE_FOLDER + File.separator + nameTraining + "_" + 
			         nameGrammar + ".counts" + " " + grammar + " " + training + " " + GRNApred.KNOWLEDGE_FOLDER + File.separator + 
				     nameTraining + "_" + nameGrammar + ".param";
		int trainingSize = (int)(GRNApred.stats.get(nameTraining.substring(0, nameTraining.lastIndexOf("_"))).getSize() * (((float)GRNApred.cvFolds - 1)/GRNApred.cvFolds));
		return runCommand(cmd, "end...", trainingSize, "%%% - Training " + nameGrammar + " with " + nameTraining, null);
	}
	
	private long testGrammar() {
		String cmd = cmdTestGrammar + (GRNApred.useSeveralModels ? " --auc" : "") + " -v --lprob " + grammar + " " + test + " " + GRNApred.PREDICTION_FOLDER + File.separator + nameTest + "_" + 
			         nameGrammar + ".sto" + " " + GRNApred.KNOWLEDGE_FOLDER + File.separator + nameTraining + "_" + nameGrammar + ".param";
		int testSize = (int)(GRNApred.stats.get(nameTraining.substring(0, nameTraining.lastIndexOf("_"))).getSize() * (1.0 / GRNApred.cvFolds));
		return runCommand(cmd, "serial master has produced a final ss for seq", testSize, "%%% - Testing " + nameGrammar + " with " + nameTest, null);
	}
	
	/**
	 * Note.- It is necessary that the different models predicted by grammars be contiguous in the .sto file.
	 * @return 
	 */
	private long computeAndStoreResults() {
		long time = System.currentTimeMillis();
		
		// read sequences from the predicted .sto file //
		List<String> sequences = GRNApred.readSequencesFromSTO(GRNApred.PREDICTION_FOLDER + File.separator + nameTest + "_" + nameGrammar + ".sto");
		Iterator<String> it = sequences.iterator();
		String [] seq; String lastID = "*";
		float [][] contacts = null; int i, j, models = 0;
		
		// iterate sequences (caution: there can be several sequences with the same ID (these are the different models) //
		while (it.hasNext()) {
			seq = GRNApred.parseSequence(it.next());
			if (!seq[0].equals(lastID)) { // new sequence //
				if (!lastID.equals("*")) {
					if (models > 1) { // divide by the number of models
						for (i=0; i<contacts.length; i++)
							for (j=0; j<contacts.length; j++)
								if (j - i > GRNApred.minSep)
									contacts[i][j] = contacts[i][j] / models;
					}
					storeSequencePredictions(lastID, contacts);
				}
				contacts = new float[seq[1].length()][seq[1].length()];
				lastID = seq[0];
				models = 0;
			}
			addContacts(seq[2], contacts);
			models++;
		}
		if (sequences.size() > 0) {
			if (models > 1) { // divide by the number of models
				for (i=0; i<contacts.length; i++)
					for (j=0; j<contacts.length; j++)
						if (j - i > GRNApred.minSep)
							contacts[i][j] = contacts[i][j] / models;
			}
			storeSequencePredictions(lastID, contacts);
		}
		else { // There was an error with this grammar, put NaNs in the contact matrix of each test sequence of this fold 
			SequenceContactPrediction scp;
			sequences = GRNApred.readSequencesFromSTO(test);
			it = sequences.iterator();
			while (it.hasNext()) {
				seq = GRNApred.parseSequence(it.next());
				scp = output.get(seq[0]);
				contacts = new float[seq[1].length()][seq[1].length()];
				for (i=0; i<contacts.length; i++)
					for (j=0; j<contacts.length; j++)
						contacts[i][j] = Float.NaN;
				storeSequencePredictions(seq[0], contacts);
			}
			
		}
		
		return System.currentTimeMillis() - time;
	}

	private void storeSequencePredictions(String seqID, float[][] contacts) {
		int i, j;
		SequenceContactPrediction scp = output.get(seqID);
		
		for (i=0; i<contacts.length; i++)
			for (j=0; j<contacts.length; j++)
				if (j - i > GRNApred.minSep)
					scp.putPrediction(i, j, nameGrammar, contacts[i][j]);
		
		if (scp.increaseGrammarsInserted())
			scp.storeInARFF(seqID);
	}

	public static void addContacts(String ss, float[][] contacts) {
		Stack<Integer> stack = new Stack<Integer>();
		int n = ss.length();
		
		for (int i = 0; i < n; i++) {
			if (ss.charAt(i) == '[' || ss.charAt(i) == '<' || ss.charAt(i) == '{' || ss.charAt(i) == '(')
				stack.push(i);
			else if (ss.charAt(i) == ']' || ss.charAt(i) == '>' || ss.charAt(i) == '}' || ss.charAt(i) == ')')
				contacts[stack.pop()][i]++;
		}
	}
	
	/**
	 * 
	 * @param cmd
	 * @param progressLine If not null, when a line of the command output contains this value, a message of progress is reported every 20%
	 * @param total If progressLine is not null, the value of progress is divided by this value
	 * @return 
	 */
	public static long runCommand (String cmd, String progressLine, int total, String progressMessage, StringBuffer output) {
		// System.out.println("Running " + processTitle + " for " + training + " with grammar " + grammar + "...");
		Process p = null;
		long time = 0; int n;
		
		if (System.getProperty("os.name").equals("Mac OS X")) {
			try {
				if (GRNApred.verbose)
					System.out.println(cmd);
				time = System.currentTimeMillis();
				p = Runtime.getRuntime().exec(cmd);
			} catch (IOException ex) {
				Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}		
		else if (System.getProperty("os.name").contains( "Linux" )) {
			try {
				if (GRNApred.verbose)
					System.out.println(cmd);
				time = System.currentTimeMillis();
				p = Runtime.getRuntime().exec(cmd);
			} catch (IOException ex) {
				Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		/*
		else if (System.getProperty("os.name").indexOf("Windows") >= 0) {
			try {
				// System.out.println(cmd);
				time = System.currentTimeMillis();
				p = Runtime.getRuntime().exec(cmd);
			} catch (IOException ex) {
				Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		*/
		InputStream is = p.getInputStream(); // p.getErrorStream(); //p.getInputStream();	         
		BufferedReader br = new BufferedReader (new InputStreamReader (is));		        
		InputStream err = p.getErrorStream();    
		BufferedReader errbr = new BufferedReader (new InputStreamReader (err));		        
		String aux; int sp;
		if (total < 5)
			sp = 1;
		else
			sp = total / 5;
		try {
			n = 1;
			aux = br.readLine();
			while (aux != null) { // Wait for the process.
				if (output != null)
					output.append( aux ).append("\n");
				if (GRNApred.verbose)
					System.out.println(aux);
				if (progressLine != null && aux.contains( progressLine )) { 
					if (n % sp == 0) {
						System.out.println(progressMessage + ": " + (100.0 * n / total) + "% (" + n + "/" + total + ")");
					}
					n++;
				}
				aux = br.readLine(); 
			}
			br.close();
			aux = errbr.readLine();
			while (aux != null) { // Wait for the process.
				if (!aux.equals(""))
					System.out.println(cmd + ": " + aux);
				aux = errbr.readLine(); 
			}
			errbr.close();
			time = System.currentTimeMillis() - time;
		} catch (IOException ex) {
			Logger.getLogger(TrainTestGrammarThread.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return time;
	}

	public long getTrainingTime() {
		return trainingTime;
	}

	public long getTestingTime() {
		return testingTime;
	}

}
