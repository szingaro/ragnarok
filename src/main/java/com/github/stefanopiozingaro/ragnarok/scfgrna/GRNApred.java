package com.github.stefanopiozingaro.ragnarok.scfgrna;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Consensus of stochastic context free grammars to RNA secondary structure prediction.
 * 
 * The program allows four operations:
 * - train   : *.sto   + *.grm   --> .param/.score/.counts [knowledge-models(grammars-trained)] + knowledge.txt
 * - predict : *.fasta + ARNtype --> .sto [predicted secondary structure]
 * - convert : *.dp --> *.sto
 * - study   : .sto + knowledge.txt --> .html
 * 
 * @author Gualberto Asencio Cortes [Bologna, Jule 9 2014]
 */
public class GRNApred {

	// SHARED CONSTANTS //
	public static final String GRAMMARS_FOLDER = "grammars";
	public static final String FOLD_FOLDER = "folds";
	public static final String KNOWLEDGE_FOLDER = "knowledge";
	public static final String PREDICTION_FOLDER = "predictions";
	public static final String ARFF_FOLDER = "arff";
	public static final int RANKING_2F1_1EXECTIME = 1;
	public static final int RANKING_LINEAR_CORR = 2;
	public static final DecimalFormat formatter = new DecimalFormat("###.####");
	
	// SHARED CONFIGURATION PARAMETERS //
	public static int cvFolds = 3; // number of folds for cross-validation //
	public static int minSep = 3; // minimum sequence separation between monomers to predict contacts //
	public static boolean useSeveralModels = false; // whether several models of each model are used (--auc) //
	public static boolean verbose = false; // output of tornado programs is shown //
	public static int maxTrainingGrammarsInExecution = 4; // number of parallel processes training grammars //
	public static int maxTestingGrammarsInExecution = 6; // number of parallel processes testing grammars //
	public static int rankingGrammarsCriteria = RANKING_2F1_1EXECTIME; // criteria to sort grammars in rankings //
	public static boolean sameFeatToTrain = true; // use same feature in training and testing fold or not // 
	
	// SHARED DATA STRUCTURES //
	public static String [] grammars; // the shared set of grammars (used by the threads) //
	public static Hashtable<String,Integer> grammarsIndices; // indices of grammars //
	public static BufferedWriter arff; // the shared ARFF file to write predictions //
	public static HashMap<String,DatasetStatistics> stats; // statistics of input sequences //

	// CONFIGURATION FOR TRAINING MODE //
	private static String inputFolder = "data", listGrammarsFilename = "grammars.txt";
	
	

	public static void main(String[] args) {
		
		// Incorrect number of input arguments
		if (args.length == 0 || args[0].equals("-help")) {
			System.out.println("\nGRNApred syntax:\n   train <options> (-help: to see help)\n" + 
					           "   predict <FASTA file> <ARN type>\n" + 
					           "   study <targets .STO file> <knowledge text file>\n" + 
					           "   convert <input file> <output file> (types are determined by file extension)\n" + 
					           "   scan <.STO folder>\n");
			System.exit(0);
		}
		
		switch ( args[0] ) {
			case "train":
				parseArgumentsTrain(args);
				train();
				break;
			case "predict":
				predict_oneSeq(args[1], args[2]);
				break;
			case "study":
				predictionStudy(args[1], args[2]);
				break;
			case "scan":
				scan(args[1]);
				break;
			case "convert":
				convert(args[1], args[2]);
				break;
			default:
				break;
		}
		
	}
	
	private static void convert(String input, String output) {
		if (input.endsWith(".dp") && output.endsWith(".sto"))
			convert_dp2sto(input, output);
	}

	/**
	 * Converts from Dot-parentheses format to Stockholm format.
	 * 
	 * @param input
	 * @param output
	 */
	private static void convert_dp2sto(String input, String output) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(input));
			BufferedWriter out = new BufferedWriter(new FileWriter(output));
			String id, seq, ss, temp;
			int i, n1 = 0, n2 = 0;
			
			temp = in.readLine();
			while (temp != null) {
				if (temp.startsWith("# File ")) { // new sequence --> retrieve id, seq and ss
					id = temp.substring(7, temp.lastIndexOf("."));
					temp = in.readLine(); temp = in.readLine(); temp = in.readLine(); temp = in.readLine(); // skip the next 4 lines 
					seq = "";
					temp = in.readLine();
					while (!temp.equals("") && temp.charAt(0) != '.' && temp.charAt(0) != '(') {
						seq += temp;
						temp = in.readLine();
					}
					seq = seq.toUpperCase(); // bases in uppercase
					if (temp.equals(""))
						temp = in.readLine();
					ss = "";
					while (!temp.equals("")) {
						ss += temp;
						temp = in.readLine();
					}
					// System.out.println(seq + "\n" + convertString(seq) + "\n");
					// id = "AAAAAAA" + p; // seq = "ACGUACGUACGUACGU"; ss = "..((.(....).))..";
					if (validSeq(seq)) {
						out.write("# STOCKHOLM 1.0\r\n\r\n" + id);
						for (i=0; i < 18 - id.length(); i++)
							out.write(" ");
						out.write(convertString(seq) + "\r\n#=GR " + id + " SS " + convertString(ss) + "\r\n//\r\n");
						n2++;
					}
					n1++;
				}
				temp = in.readLine();
			}			
			out.close();
			in.close();
			System.out.println("Original DP: " + n1 + " seqs.\nOutput STO: " + n2 + " seqs (" + (n1-n2) + " seqs removed because non-standard nucleotides).");
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	private static boolean validSeq (String seq) {
		boolean res = true;
		int i = 0, n = seq.length();
		
		while (i < n && res) {
			if (seq.charAt(i) != 'A' && seq.charAt(i) != 'C' && seq.charAt(i) != 'G' && seq.charAt(i) != 'U')
				res = false;
			i++;
		}		
		
		return res;
	}
	
	private static String convertString (String in) {
		String out = "";
		int i;
		
		for (i = 0; i < in.length(); i++) {
			if (in.charAt(i) == 'A')
				out += 'A';
			else if (in.charAt(i) == 'C')
				out += 'C';
			else if (in.charAt(i) == 'G')
				out += 'G';
			else if (in.charAt(i) == 'U')
				out += 'U';
			else if (in.charAt(i) == '(')
				out += '(';
			else if (in.charAt(i) == ')')
				out += ')';
			else if (in.charAt(i) == '.')
				out += '.';
		}		
		
		return out;
	}

	public static void train () {
		
		TrainTestGrammarThread [][] threads;
		FeatureSelectionThread [] threads2;
		String [][] foldFilenames;
		String [] inputFiles = (new File(inputFolder)).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".sto");
			}
		});
		String [] grammarFiles = getGrammarsToUse(listGrammarsFilename);
		int i = 0, f, g, ng; long time;
		HashMap<String,SequenceContactPrediction> output;
		List<String> sequences;
		stats = new HashMap<String,DatasetStatistics> ();
		DatasetStatistics stat;
		int folds;
		
		ng = grammarFiles.length;		
		threads2 = new FeatureSelectionThread[inputFiles.length];
		
		deleteFolder(new File(FOLD_FOLDER));
		//deleteFolder(new File(KNOWLEDGE_FOLDER));
		deleteFolder(new File(PREDICTION_FOLDER));
		new File(FOLD_FOLDER).mkdir();
		new File(KNOWLEDGE_FOLDER).mkdir();
		new File(PREDICTION_FOLDER).mkdir();
		new File(ARFF_FOLDER).mkdir();
		
		grammars = new String[ng];
		grammarsIndices = new Hashtable<String,Integer>(ng);
		for (g = 0; g < ng; g++) { // for each grammar
			grammars[g] = grammarFiles[g].substring(0, grammarFiles[g].lastIndexOf(".")); // removing file extension //
			grammarsIndices.put(grammars[g], g);
		}
		
		time = System.currentTimeMillis();
		System.out.println("GRNApred - Training mode\n");
		for (String file : inputFiles) { // for each input file (.sto)
			sequences = readSequencesFromSTO(inputFolder + File.separator + file);
			output = prepareTrainingOutput(sequences, false);
			stat = calculateDatasetStats(output);
			stats.put(file.replace(".sto", ""), stat);
			System.out.println(">>> - Dataset: " + file + " " + stat);
			TrainTestGrammarThread.output = output;
			createARFF(ARFF_FOLDER + File.separator + file.replace(".sto", ".arff")); // it puts a BufferedWriter as static field in this class
			if (stat.getSize() >= cvFolds)
				folds = cvFolds;
			else
				folds = stat.getSize();
			threads = new TrainTestGrammarThread[folds][ng];
			foldFilenames = divideSTOinFolds(sequences, inputFolder + File.separator + file, inputFiles, folds);
			for (f = 0; f < folds; f++) { // for each fold into the input file
				for (g = 0; g < ng; g++) { // for each grammar
					threads[f][g] = new TrainTestGrammarThread(foldFilenames[f], GRAMMARS_FOLDER + File.separator + grammarFiles[g]);
					threads[f][g].start();
				}
			}
			for (f = 0; f < folds; f++) { // for each fold into the input file
				for (g = 0; g < ng; g++) { try {
					threads[f][g].join(); // wait for threads
					} catch ( InterruptedException ex ) {
						Logger.getLogger( GRNApred.class.getName() ).log( Level.SEVERE, null, ex );
					}
				}
			}
			try {
				if (arff != null)
					arff.close(); // close the current arff file
			} catch (IOException ex) {
				Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
			}
			
			// once the arff is generated then produce the attribute ranking in parallel (and it performs the consolidation of .param/.counts of folds --> grm-parse does not work) //
			threads2[i] = new FeatureSelectionThread(ARFF_FOLDER + File.separator + file.replace(".sto", ".arff"), threads);
			threads2[i].start();
			i++;
		}
		for (i = 0; i < threads2.length; i++) {
			try {
				threads2[i].join(); // wait for threads
			} catch ( InterruptedException ex ) {
				Logger.getLogger( GRNApred.class.getName() ).log( Level.SEVERE, null, ex );
			}
		}
		storeResults(stats);
		time = System.currentTimeMillis() - time;
		System.out.println("Training process has taken " + (time / 60000.0) + " mins (" + (time / 1000.0) + " secs).");
	}

	private static void storeResults(HashMap<String,DatasetStatistics> stats) {
		boolean exists = false;
		try {
			System.out.println(FeatureSelectionThread.ranking);
			if (new File(KNOWLEDGE_FOLDER + File.separator + "knowledge.txt").exists())
				exists = true;
			try (BufferedWriter output = new BufferedWriter(new FileWriter(KNOWLEDGE_FOLDER + File.separator + "knowledge.txt", true))) {
				Iterator<String> it = FeatureSelectionThread.ranking.keySet().iterator();
				Iterator<PredictionPerformance> it2;
				PredictionPerformance pp;
				DatasetStatistics stat;
				String dataset;
				
				if (!exists)
					output.write("Dataset\t\t\tType\t\tSize\tMinLength\tAvgLength\tStdevLength\tMaxLength\tRanking\r\n");
				while (it.hasNext()) {
					dataset = it.next();
					stat = stats.get(dataset);
					output.write(dataset + "\t" + getDatasetType(dataset) + "\t" + stat.getSize() + "\t" + stat.getMinimumLength() + "\t" + GRNApred.formatter.format(stat.getAverageLength()).replace(',','.') + "\t" + GRNApred.formatter.format(stat.getStdevLength()).replace(',','.') + "\t" +
						stat.getMaximumLength() + "\t" + FeatureSelectionThread.ranking.get(dataset) + "\r\n");
				}
			}
		} catch (IOException ex) {
			Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static String getDatasetType(String dataset) {
		if (dataset.contains( "length" ) || dataset.contains( "LENGTH" ))
			return "byLength";
		else if (dataset.contains( "gc" ) || dataset.contains( "GC" ))
			return "byGCcontent";
		else if (dataset.contains( "type" ) || dataset.contains( "TYPE" ))
			return "bySeqType";
		return "";
	}

	private static String[] getGrammarsToUse (String listGrammarsFilename) {
		String [] res = null;
		List<String> temp = new LinkedList<String> ();
		BufferedReader in;
		String aux;
		
		try {
			in = new BufferedReader(new FileReader(listGrammarsFilename));
			aux = in.readLine();
			while (aux != null) {
				temp.add(aux);
				aux = in.readLine();
			}
			in.close();
			int i = 0;
			res = new String[temp.size()];
			Iterator<String> it = temp.iterator();
			while (it.hasNext()) {
				res[i] = it.next();
				i++;
			}
		} catch (IOException ex) {
			Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
		}		
		
		return res;
	}

	private static void createARFF(String filename) {
		try {
			arff = new BufferedWriter(new FileWriter(filename));
			arff.write(arffHeader(filename));
		} catch (IOException ex) {
			Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
		}		
	}
	
	private static String arffHeader (String filename) {
		String res = "@relation " + filename.substring(filename.lastIndexOf(File.separator)+1, filename.lastIndexOf('.')) + "\r\n\r\n";
		
		res += "@attribute seqid string\r\n";
		res += "@attribute i numeric\r\n";
		res += "@attribute j numeric\r\n";
		for (int g = 0; g < grammars.length; g++) {
			res += "@attribute " + grammars[g] + " numeric\r\n";
		}
		res += "@attribute class {0,1}\r\n";
		res += "\r\n@data\r\n";
		
		return res;
	}

	private static HashMap<String, SequenceContactPrediction> prepareTrainingOutput (List<String> sequences, boolean onlyLength) {
		HashMap<String,SequenceContactPrediction> res = new HashMap<String,SequenceContactPrediction> (sequences.size());
		Iterator<String> it = sequences.iterator();
		SequenceContactPrediction scp;
		String seq; String [] aux;
		
		while (it.hasNext()) {
			seq = it.next();
			aux = parseSequence(seq);
			// System.out.println(aux[0] + "  " + aux[1] + "  " + aux[2]);
			if (onlyLength)
				scp = new SequenceContactPrediction(aux[1].length());
			else
				scp = new SequenceContactPrediction(aux[1].length(), aux[2]);
			res.put(aux[0], scp);
		}
		
		return res;
	}
	
	/**
	 * Parses a STO sequence and returns a vector with 3 strings:
	 * [0] The sequence ID
	 * [1] The sequence
	 * [2] The secondary structure
	 * 
	 * @param sequence
	 * @return 
	 */
	public static String [] parseSequence (String sequence) {
		String [] res, res2;
		String aux; int pos, pos2, posBase;
		
		pos = sequence.indexOf("#=GR ") + 5; // position of the line which includes the secondary structure ("#=GR " has 5 characters)
		aux = sequence.substring(pos, sequence.indexOf("\n", pos));
		res = aux.split(" "); // Usually text fragments are like: ["RF00001_A","SS","(((((((((,,,,<<-<<<<<--"] //
		pos2 = sequence.indexOf("\n" + res[0]) + 1; // position of the line which includes the sequence (this line starts with the sequence ID previously retrieved)
		res[1] = sequence.substring(pos2, sequence.indexOf("\n", pos2)); // the complete line that contains the sequence
		res[1] = res[1].substring(res[1].lastIndexOf(" ") + 1);

		posBase = sequence.indexOf("#=GR ") + 5; 
		pos = sequence.indexOf("#=GR ", posBase); // The possible second "#=GR " line in the argument 'sequence'
		while (pos >= 0) { // Search for possible more fragments of the same sequence on different lines
			pos += 5;
			aux = sequence.substring(pos, sequence.indexOf("\n", pos));
			res2 = aux.split(" "); // Usually text fragments are like: ["RF00001_A","SS","(((((((((,,,,<<-<<<<<--"] //
			res[2] += res2[2]; // append a new fragment of secondary structure
			pos2 = sequence.indexOf("\n" + res2[0], posBase) + 1; // position of the line which includes the sequence (this line starts with the sequence ID previously retrieved)
			res2[1] = sequence.substring(pos2, sequence.indexOf("\n", pos2)); // the complete line that contains the sequence
			res[1] += res2[1].substring(res2[1].lastIndexOf(" ") + 1); // append a new fragment of sequence
			posBase = pos;
			pos = sequence.indexOf("#=GR ", pos); // The next "#=GR " line
		}
		
		return res;
	}

	public static void predictionStudy (String targetsSto, String knowledgeFilename) {		
		String output;
		long time = System.currentTimeMillis();
		int i, n;
		
		System.out.println("GRNApred - Prediction Study mode\n");
		
		PredictionStudy study = new PredictionStudy(targetsSto, knowledgeFilename, new String []{"VRNA"}, 3);
		n = study.getNumPredictionsOverAllTargets();
		PredictionStudyThread [] threads = new PredictionStudyThread[n];
		
		// verbose = true;
		for (i = 0; i < n; i++) {
			threads[i] = new PredictionStudyThread(study, i);
			threads[i].start();
		}
		for (i = 0; i < n; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		output = targetsSto.substring(0, targetsSto.lastIndexOf('.')) + "_" + knowledgeFilename.substring(knowledgeFilename.lastIndexOf(File.separator) + 1, knowledgeFilename.lastIndexOf('.')) + ".html";
		study.produceHTML(output);
		
		System.out.println("Output generated in " + output + " (" + ((System.currentTimeMillis() - time) / 1000) + " secs)\n");
	}

	public static void predict_oneSeq (String targetFasta, String rnaType) {
		String line, bestDatasetRanking = "", bestDataset = "", bestGrammar, cmd, resultFilename;
		String [] aux, counts;
		int length = targetFasta.length();
		float diff, minDiff = Integer.MAX_VALUE;
		long time = System.currentTimeMillis();
		
		System.out.println("GRNApred - Prediction mode\n");
		try {
			try ( // select best dataset-ranking from knowledge model depending on target sequence //
				BufferedReader km = new BufferedReader(new FileReader(KNOWLEDGE_FOLDER + File.separator + "knowledge.txt"))) {
				line = km.readLine(); // skipping the header row //
				line = km.readLine();
				while (line != null) {
					aux = line.split("\t");
					if (aux[0].contains(rnaType)) {
						diff = Math.abs(length - Float.parseFloat(aux[3])); // average length of the dataset from the knowledge //
						if (diff < minDiff) {
							minDiff = diff;
							bestDatasetRanking = aux[aux.length-1];
							bestDataset = aux[0];
						}
					}
					line = km.readLine();
				}
			} // skipping the header row //
		} catch (IOException ex) {
			Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		if (bestDatasetRanking.equals("")) {
			System.out.println("!!! - There is no suitable model to predict this target sequence, because its RNA type (" + rnaType + ") is not included in the knowledge file.");
			System.exit(0);
		}
		else { // a most suitable grammar/dataset is selected //
			bestDatasetRanking = bestDatasetRanking.substring(1, bestDatasetRanking.length() - 1); // remove [ ] endings
			aux = bestDatasetRanking.split(",");
			bestGrammar = aux[0].substring(0, aux[0].indexOf("("));
			System.out.println("The grammar selected to predict the target sequence is " + bestGrammar);
			
			// prepare the sentence to execute the predictor using the best grammar and its knowledge (summing on counts from all CV-folds) //
			resultFilename = targetFasta.replace(".fasta", "_predicted.sto");
			cmd = TrainTestGrammarThread.cmdTestGrammar + " --infmt fasta --count " + GRAMMARS_FOLDER + File.separator + bestGrammar + ".grm " + targetFasta + " " + 
				  resultFilename;			
			final String bestDataset2 = bestDataset, bestGrammar2 = bestGrammar;
			counts = (new File(GRNApred.KNOWLEDGE_FOLDER)).list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.startsWith(bestDataset2) && name.endsWith(bestGrammar2 + ".counts");
				}
			});
			for (String file : counts) {
				cmd += " " + GRNApred.KNOWLEDGE_FOLDER + File.separator + file;
			}
			
			// execute the prediction //
			// System.out.println(cmd);
			TrainTestGrammarThread.runCommand(cmd, "-----------------------------------", 100, "%%% - Predicting target", null);
			System.out.println("Sequence predicted! The result is available in the file " + resultFilename + " (" + ((System.currentTimeMillis() - time) / 1000) + " secs).");
		}		
	}

	public static List<String> readSequencesFromSTO (String stoFilename) {
		// Read complete input stoFilename and put sequences into a list //
		List<String> sequences = new ArrayList<String> ();
		String temp, aux = "";
		
		try {
			try (BufferedReader br = new BufferedReader(new FileReader(stoFilename))) {
				temp = br.readLine();
				while (temp != null) {
					//if (temp.startsWith("# STOCKHOLM 1.0"))
					//	aux = temp;
					//else
					aux += temp + "\n";
					if (temp.startsWith("//")) {
						sequences.add(aux);
						aux = "";
					}
					temp = br.readLine();
				}
			}
		} catch (IOException ex) {
			System.out.println("!!! - The file " + stoFilename + " can not be opened.");
		}	
		
		return sequences;
	}
	
	/**
	 * Returns a matrix of filenames, each row is a fold, there is 2 columns: training and test.
	 * It creates this matrix files in the FOLD_FOLDER folder.
	 * @param string
	 * @return 
	 */
	private static String[][] divideSTOinFolds(List<String> sequences, String stoFilename, String [] stos, int folds) {
		String [][] res = new String [folds][2];
		int [] indices;
		int i, f, n;
		BufferedWriter bw;
		List<String> sequences2;
		
		for (f = 0; f < folds; f++) {
			res[f][0] = FOLD_FOLDER + File.separator + stoFilename.substring(stoFilename.lastIndexOf(File.separator)+1, stoFilename.lastIndexOf('.')) + "_Train" + (f+1) + "-" + cvFolds + ".sto";
			res[f][1] = FOLD_FOLDER + File.separator + stoFilename.substring(stoFilename.lastIndexOf(File.separator)+1, stoFilename.lastIndexOf('.')) + "_Test" + (f+1) + "-" + cvFolds + ".sto";
		}
		
		n = sequences.size();		
		if (n == 0) {
			System.err.println("No sequences found in " + stoFilename + ". Aborting.");
			System.exit(1);
		}
		
		if (sameFeatToTrain) {
			sequences2 = sequences;
		}
		else {
			sequences2 = new ArrayList<String>();
			// AQUI...

		}
		// indices  = new int [sequences.size()];
		// randomizeVector(indices);
		
		// Distribute sequences into fold files //
		int spf = n / folds; // spf is: sequences per fold
		for (f = 0; f < folds; f++) {
			try {
				// Saving test of fold f into a .sto file //
				bw = new BufferedWriter(new FileWriter(res[f][1]));
				for (i = f * spf; i < (f+1) * spf; i++) {
					bw.write(sequences.get(i));								
				}
				bw.close();
				// Saving training of fold f into a .sto file //
				bw = new BufferedWriter(new FileWriter(res[f][0]));
				for (i = 0; i < f * spf; i++) { // first part
					bw.write(sequences2.get(i));								
				}
				for (i = (f+1) * spf; i < n; i++) { // second part
					bw.write(sequences2.get(i));								
				}
				bw.close();
			} catch (IOException ex) {
				Logger.getLogger(GRNApred.class.getName()).log(Level.SEVERE, null, ex);
			}			
		}
		
		return res;
	}
	
	private static void randomizeVector(int[] indices) {
		java.util.Random rnd = new java.util.Random();
		// int i = 
		
	}

	public static void deleteFolder(File folder) {
		File[] files = folder.listFiles();
		if(files!=null) { //some JVMs return null for empty dirs
			for(File f: files) {
				if(f.isDirectory()) {
					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
		folder.delete();
	}

	private static DatasetStatistics calculateDatasetStats(HashMap<String,SequenceContactPrediction> output) {
		Iterator<String> it = output.keySet().iterator();
		SequenceContactPrediction scp;
		String seqID;
		int sum = 0, n = 0, min = Integer.MAX_VALUE, max = 0;
		float avg, stdev = 0;
		
		while (it.hasNext()) {
			seqID = it.next();
			scp = output.get(seqID);
			// System.out.println(seqID + ": " + scp.getLength());
			sum += scp.getLength();
			if (scp.getLength() > max)
				max = scp.getLength();
			if (scp.getLength() < min)
				min = scp.getLength();
			n++;
		}
		avg = ((float)sum) / n;
		
		it = output.keySet().iterator();
		while (it.hasNext()) {
			seqID = it.next();
			scp = output.get(seqID);
			stdev += Math.pow(scp.getLength() - avg, 2);
		}
		stdev = (float)Math.sqrt(stdev / n);

		return new DatasetStatistics(avg, n, min, max, stdev);
	}

	private static void scan(String stoFolder) {
		String [] inputFiles = (new File(stoFolder)).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".sto");
			}
		});
		List<String> sequences;
		DatasetStatistics stat;
		HashMap<String,SequenceContactPrediction> output;
		
		grammars = new String[1];
				
		System.out.println("GRNApred - Scanning mode\n");
		for (String file : inputFiles) { // for each input file (.sto)
			sequences = readSequencesFromSTO(stoFolder + File.separator + file);
			output = prepareTrainingOutput(sequences, true);
			stat = calculateDatasetStats(output);
			System.out.println(">>> - Dataset: " + file + " " + stat);
		}

	}

	private static void parseArgumentsTrain (String [] args) {

		String help = "\nGRNApred Training Mode usage:\n\n" +
				"  GRNApred train [-input inputFolder] [-g gramListFile] [-verbose] [-cv numFolds] [-ms minSep] [-nrg numTrainGram] [-ntg numTestGram] [-sft sameFeatToTrain] [-severalModels]\n\n" +
				"  where:\n" +
				"    inputFolder     path to the folder of .STO training files (default = data)\n" +
				"    gramListFile    text file with list of grammars to train (default = grammars.txt)\n" +
				"    numFolds        number of folds to perform cross validation (default = 3)\n" +
				"    minSep          minimum separation between nucleotides to predict hydrogen bonds (default = 3)\n" +
				"    numTrainGram    number of training grammars processes executed in parallel (default = 4)\n" +
				"    numTestGram     number of testing grammars processes executed in parallel (default = 6)\n" +
				"    sameFeatToTrain use same feature in training and testing fold or not (default = true)\n\n";
		
		if (args.length == 2 && (args[1].equalsIgnoreCase("-h") || args[1].equalsIgnoreCase("-help"))) {
			System.out.println(help);
			System.exit(0);
		}
		
		int i = 1;
		while (i < args.length) { // read arguments
			switch ( args[i] ) {
				case "-input":
					inputFolder = args[++i];
					break;
				case "-g":
					listGrammarsFilename = args[++i];
					break;
				case "-cv":
					cvFolds = Integer.parseInt(args[++i]);
					break;
				case "-ms":
					minSep = Integer.parseInt(args[++i]);
					break;
				case "-nrg":
					maxTrainingGrammarsInExecution = Integer.parseInt(args[++i]);
					break;
				case "-ntg":
					maxTestingGrammarsInExecution = Integer.parseInt(args[++i]);
					break;
				case "-sft":
					sameFeatToTrain = Boolean.parseBoolean(args[++i]);
					break;
				case "-verbose":
					verbose = true;
					break;
				case "-severalModels":
					useSeveralModels = true;
					break;
				default:
					// illegal syntax
					System.out.println("Unrecognized option!\n" + help);
					System.exit(1);
			}
			i++;
		}
	}	
}
